# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    unique_list = ""
    for i in s:
        if i not in unique_list:
            unique_list += i
    return unique_list


print(remove_duplicate_letters("abccbad")) #"abccbad"
print(remove_duplicate_letters("abcabc")) #abc