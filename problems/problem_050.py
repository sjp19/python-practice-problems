# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(single_list):
    list_length = len(single_list)
    list_half = list_length//2
    if list_length % 2 == 0:
        return single_list[:list_half], single_list[list_half:]
    list_half += 1
    return single_list[:list_half], single_list[list_half:]

print(halve_the_list([1, 2, 3, 4]))