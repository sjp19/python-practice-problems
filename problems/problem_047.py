# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    symbols = ['$','@','!']
    result = True
    if len(password) > 13 or len(password) < 5:
        result = False
    if not any(char in symbols for char in password):
        result = False
    if not any(char.isdigit() for char in password):
        result = False
    if not any(char.isupper() for char in password):
        result = False
    if not any(char.isupper() for char in password):
        result = False
    if not any(char.isalpha() for char in password):
        result = False
    return result

print(check_password('a3!M')) #fail
print(check_password('aaaaa')) #fail 
print(check_password('a3!!sasaaaa')) #fail