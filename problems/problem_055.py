# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman(num):
    if num < 11 and num > 0:
        if num == 1:
            return "I"
        if num == 2:
            return "II"
        if num == 3:
            return "III"
        if num == 4:
            return "IV"
        if num == 5:
            return "V"
        if num == 6:
            return "VI"
        if num == 7:
            return "VII"
        if num == 8:
            return "VIII"
        if num == 9:
            return "IX"
        if num == 10:
            return "X"
    return "Invalid number"

print(simple_roman(0))