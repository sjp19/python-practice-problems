# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    pasta_ingredients = ["flour", "egg", "oil"]
    for food in ingredients:
        if food in pasta_ingredients:
            return True
        else:
            return False

food_list = ['flour', 'oil', 'egg']
print(can_make_pasta(food_list))