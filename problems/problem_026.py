# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    #average >= 90 A
    #average >= 80 < 90 B
    #average >= 70 < 80 C
    #average >= 60 < 70 D
    #below that is F
    grades = []
    for num in values: 
        if num >= 90:
            grades.append("A")
        if num >= 80 and num < 90:
            grades.append("B")
        if num >= 70 and num < 80:
            grades.append("C")
        if num >= 60 and num < 70:
            grades.append("D")
        if num < 60:
            grades.append("F")
    return grades


print(calculate_grade([90, 99, 60, 1, 88, 75])) #A, A, D, F, B, C