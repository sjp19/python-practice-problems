# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7


def num_two_numbers(one_num, two_num):
    return one_num + two_num if type(one_num) == int and type(two_num) == int else None
    #     return one_num + two_num
    # return None

print(num_two_numbers(8, 9))