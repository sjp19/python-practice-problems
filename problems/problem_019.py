# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of a rectangle
#   rect_y: The top of a rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * x is less than or equal to rect_x + rect_width
    # y is greater than or equal to rect_y
#   * y is less than or equal to rect_y + rect_height

####WHAT IS RECT_X AND WHAT IS RECT_Y?????????????
def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    if x >= rect_x and x <= rect_x + rect_width and y >= rect_y and y <= rect_y + rect_height:
        return True
    return False



print(is_inside_bounds(9, 5, 5, 3, 4, 2)) #True
#x = 9
#y = 5
#rect_x = 5
#rect_y = 3
#rect_width = 4
#rect_height = 2
print(is_inside_bounds(26, 5, 25, 4, 54, 34)) #True
print(is_inside_bounds(1, 10, 25, 4, 54, 88)) #False