# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    result = 0
    if values == []:
        return None
    for num in values:
        result += num
    return result

print(calculate_sum([1, 2, 3]))