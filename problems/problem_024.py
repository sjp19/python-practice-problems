# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    result = 0
    if values == []:
        return None
    for num in values:
        result += num
    result = result/len(values)
    return result


print(calculate_average([1, 2, 3]))